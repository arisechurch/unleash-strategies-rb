$:.push File.expand_path("../lib", __FILE__)
require "unleash_strategies/version"

Gem::Specification.new do |s|
  s.name        = "unleash_strategies"
  s.version     = UnleashStrategies::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["ARISE Church"]
  s.email       = ["it@arisechurch.com"]
  s.homepage    = "https://arisechurch.com"
  s.summary     = ""
  s.description = ""
  s.license     = ""
  s.required_ruby_version = ">= 2.0"

  s.add_runtime_dependency 'unleash', '~> 3.2', '>= 3.2.0'

  s.files         = `find *`.split("\n").uniq.sort.select { |f| !f.empty? }
  s.executables   = []
  s.require_paths = ["lib"]
end
