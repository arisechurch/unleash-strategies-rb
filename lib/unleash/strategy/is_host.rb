require "unleash"

module Unleash
  module Strategy
    class IsHost < Base
      PARAM = "host".freeze

      def name
        "isHost"
      end

      # requires: params['host'], context.host,
      def is_enabled?(params = {}, context = nil)
        return false unless params.is_a?(Hash) && params.key?(PARAM)
        return false unless params.fetch(PARAM, nil).is_a? String
        return false unless context.class.name == "Unleash::Context"
        return false unless context.properties[:host].present?

        params[PARAM].strip.downcase == context.properties[:host]
      end
    end
  end
end
