require 'unleash'

module Unleash
  module Strategy
    class IsEnvironment < Base
      PARAM = 'environment'.freeze

      def name
        'isEnvironment'
      end

      # requires: params['userIds'], context.user_id,
      def is_enabled?(params = {}, context = nil)
        return false unless params.is_a?(Hash) && params.key?(PARAM)
        return false unless params.fetch(PARAM, nil).is_a? String
        return false unless context.class.name == 'Unleash::Context'

        params[PARAM].strip.downcase == context.environment
      end
    end
  end
end
